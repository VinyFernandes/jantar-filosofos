##semafaro.H

Um dos mecanismos computacionais que podemos usar para que não haja inconsistência de recursos são os semáforos.

Um semáforo é uma variável, um mecanismo de sincronização sem espera ativa. Esta variável pode ser manipulada através de duas primitivas atómicas, isto é, que não podem ser interrompidas por processos.

A sua função é controlar o acesso a recursos partilhados num ambiente multitarefa. A invenção desse tipo de variável é atribuída a Edsger Dijkstra.

Quando declaramos um semáforo, o seu valor indica quantos processos (ou threads) podem ter acesso ao recurso partilhado. Assim, para partilhar um recurso o leitor deve verificar qual o valor do semáforo para saber qual a ação a executar.

Assim, e de forma a consolidar os conhecimentos, as principais operações sobre semáforos são:

##Inicialização
Recebe como parâmetro um valor inteiro indicando a quantidade de processos que podem aceder a um determinado recurso.

##Operação wait
Decrementa o valor do semáforo. Se o semáforo está com valor zero, o processo é posto em espera. Para receber um sinal, um processo executa o wait e bloqueia se o semáforo impedir a continuação da sua execução.

As operações de incrementar e decrementar devem ser operações atômicas, ou indivisíveis, isto é, a instrução deve ser executada na totalidade sem que haja interrupções. Enquanto um processo estiver a executar uma dessas duas operações, nenhum outro processo pode executar outra operação nesse mesmo semáforo, devendo esperar que o primeiro processo encerre a sua operação. Essa obrigação evita condições de disputa entre as várias threads.